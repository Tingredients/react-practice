import React from 'react'
import { Formik, Form, FieldArray } from 'formik';
import { DatePicker, TimePicker } from 'antd';
import moment from 'moment'

const inspections = []

const onChange = (arrayHelpers, index, dateString) => {
  arrayHelpers.replace(index, {date: moment(new Date(dateString))})
}

const InspectionList = () => (
  <div>
    <h1>Inspection List</h1>
    <Formik
      initialValues={{ inspections: inspections }}
      onSubmit={values =>
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
        }, 500)
      }
      render={({ values }) => (
        <Form>
          <FieldArray
            name="inspections"
            render={arrayHelpers => (
              <div>
                <button
                  type="button"
                  onClick={() => arrayHelpers.push({date: new Date()})} // insert an empty string at a position
                >
                  Add a inspection time
                </button>
                {values.inspections && values.inspections.length > 0 ? (
                  values.inspections.map((inspection, index) => (
                    <div key={index}>
                    <DatePicker value={moment(new Date(inspection.date))} onChange={(date, dateString) => {onChange(arrayHelpers, index, dateString)}}/>
                    <button
                      type="button"
                      onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                    >
                      -
                    </button>
                    </div>
                  ))
                ) : (<span></span>)}
                <div>
                  <button type="submit">Submit</button>
                </div>
              </div>
            )}
          />
        </Form>
      )}
    />
  </div>
);

class MySchedule extends React.Component{

  render() {
    return (
      InspectionList()
    )
  }
}

export default MySchedule
