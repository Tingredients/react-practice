import React from 'react'

const withStorage = (WrapperComponent) => {
  class HOC extends React.Component {
    state = {
      localStaorageAvailable: false
    }

    componentDidMount() {
      this.checkLocalStorageExists();
    }

    checkLocalStorageExists() {
      const testKey = 'test';

      try {
        localStorage.setItem(testKey, testKey)
        localStorage.removeItem(testKey)

        this.setState({
          localStaorageAvailable: true
        })
      } catch(e) {
        this.setState({
          localStorageAvailable: false
        })
      }
    }

    load = (key) => {
      if (this.state.localStorageAvailable) {
        return localStorage.getItem(key);
      }

      return null;
    }

    save = (key, data) => {
      if (this.state.localStorageAvailable) {
        localStorage.setItem(key, data);
      }
    }

    remove = (key) => {
      if (this.state.localStorageAvailable) {
        localStorage.removeItem(key);
      }
    }

    render() {
      return (
        <WrappedComponent
          load={this.load}
          save={this.save}
          remove={this.remove}
          {...this.props}
        />
      );
    }
  }

  return HOC;
}

export default withStorage
