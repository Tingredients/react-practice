import React from 'react'
import {connect} from 'react-redux'
import {add, increment} from './actions'

class Add extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      items: []
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      value: e.target.value
    })
  }

  addItem = (event) => {
    event.preventDefault();
    this.props.add(this.state.value)
    this.setState({
      value: ''
    })
  }
  render(){
    let counter = 0
    
    return <div>
      {/*<input type="text" value={this.state.value} onChange={this.handleChange}/>
      <button onClick={this.addItem}>Add</button>*/}

      <hr />
      {this.props.counter}
      <button onClick={() => {this.props.increment(counter+1)}}>+</button>
    </div>
  }
}

const mapStateToProps = (state) => ({
  items: state.todos,
  counter: state.todos
})

const mapDispatchToProps = (dispatch) => {
  return {
    // add: (item) => dispatch(add(item)),
    increment: (counter) => dispatch(increment(counter))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Add)
