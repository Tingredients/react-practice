import React from 'react'
import { Formik, Form, Field, FieldArray } from 'formik';

const Friends = [
  {firstName: 'Steve', lastName: 'Chen'},
  {firstName: 'Jessie', lastName: 'Zhang'}
]

const FriendList = () => (
  <div>
    <h1>Friend List</h1>
    <Formik
      initialValues={{ friends: Friends }}
      onSubmit={values =>
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
        }, 500)
      }
      render={({ values }) => (
        <Form>
          <FieldArray
            name="friends"
            render={arrayHelpers => (
              <div>
                <button
                  type="button"
                  onClick={() => arrayHelpers.push('')} // insert an empty string at a position
                >
                  +
                </button>
                {values.friends && values.friends.length > 0 ? (
                  values.friends.map((friend, index) => (
                    <div key={index}>
                    <Field name={`friends[${index}].firstName`} />
                    <Field name={`friends.${index}.lastName`} />
                      <button
                        type="button"
                        onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                      >
                        -
                      </button>
                    </div>
                  ))
                ) : (
                  <button type="button" onClick={() => arrayHelpers.push('')}>
                    {/* show this when user has removed all friends from the list */}
                    Add a friend
                  </button>
                )}
                <div>
                  <button type="submit">Submit</button>
                </div>
              </div>
            )}
          />
        </Form>
      )}
    />
  </div>
);

class MyForm extends React.Component{

  render() {
    return (
      FriendList()
    )
  }
}

export default MyForm
