import React, { Component } from 'react';
import {connect} from 'react-redux'
import {Row, Col} from 'antd'
import 'antd/dist/antd.css'
import './App.css';
// import Add from './add'
// import List from './list'
// import MyForm from './form'
// import MySchedule from './datetimepicker'

import EditForm from './edit'

class App extends Component {
  state = {
      isEdit: false
  }

  edit(e, isEdit) {
    e.preventDefault()
    this.setState({
      isEdit: true
    })
    setTimeout(() => {
      console.log(this.state.isEdit)
    })
  }

  editForm() {
    if (this.state.isEdit) {
      return <div>
        <EditForm />
        <button onClick={() => { this.setState({isEdit: false})} }>Cancel</button>
      </div>
    } else {
      return <p>My Content is here</p>
    }
  }

  render() {
    return (
      <div className="App">
        <Row>
          <Col>{this.editForm()}</Col>
          <hr />
          <Col>
            <button onClick={(e) => {this.edit(e, true)}}>Edit</button>
          </Col>
        </Row>

        {/*<Add />
        <List items={this.props.items} />


        <MyForm />
        <MySchedule />*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  items: state.todos
})

export default connect(
  mapStateToProps
)(App)
