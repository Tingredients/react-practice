export const add = (item) => ({
  type: 'add',
  item
})

export const increment = (value) => ({
  type: 'ADD',
  counter: value
})
