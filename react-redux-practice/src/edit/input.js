import React from 'react'
import FormItem from 'antd/lib/form/FormItem';
import { Input } from 'antd';

const input = (values, errors, touched, handleChange, handleBlur) => {
  return <FormItem
            labelCol={{xs: {span: 24}, sm: {span: 24}}}
            wrapperCol={{xs: {span: 24}, sm: {span: 24}}}
            label={'Name'}
            help={touched.name && errors.name}
            validateStatus={
                touched.address && errors.name ? 'error' : undefined
            }>
            <Input
                id={'address'}
                name={'name'}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
            />
        </FormItem>
}

export default input;
