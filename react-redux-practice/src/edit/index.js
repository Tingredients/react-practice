import React from 'react';
import { Formik } from 'formik';
import {Form, Col, Row} from 'antd';
import myInput from './input'

class EditForm extends React.Component{
  initialValues() {
      return {
        name: 'Steve'
      }
  }
  render() {
    return (
      <div>
        <h1>My Form</h1>
        <Formik
          initialValues={this.initialValues()}
          onSubmit={(values, actions) => {
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              actions.setSubmitting(false);
            }, 500);
          }}
          render={({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit
            }) => (
            <Form onSubmit={handleSubmit}>
              {myInput(values, errors, touched, handleChange, handleBlur)}
              <Row type={'flex'} gutter={16}>
                <button type="submit">Submit</button>
              </Row>
            </Form>
          )}
        />
      </div>
    )
  }
}

export default EditForm
