import React from 'react'
import FormItem from 'antd/lib/form/FormItem';
import { Input, Col, Row, Button } from 'antd';
import {connect} from 'react-redux'
import {next, back} from './actions'
import DateTimePicker from './datetimepicker'

const backBtn = (e, props) => {
  e.preventDefault();
  props.dispatch(back())
}

const nextBtn = (e, props) => {
  e.preventDefault();
  localStorage.setItem('details', JSON.stringify(props.values))
  props.dispatch(next())
}

const Form1 = (props) => {
  return <React.Fragment>
      <FormItem
          labelCol={{xs: {span: 24}, sm: {span: 24}}}
          wrapperCol={{xs: {span: 24}, sm: {span: 24}}}
          help={props.touched.firstName && props.errors.firstName}
          validateStatus={
              props.touched.firstName && props.errors.firstName ? 'error' : undefined
          }>
          <Input
              id={'firstName'}
              name={'firstName'}
              placeholder={'First Name'}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.firstName}
          />
      </FormItem>

      <FormItem
          labelCol={{xs: {span: 24}, sm: {span: 24}}}
          wrapperCol={{xs: {span: 24}, sm: {span: 24}}}
          help={props.touched.lastName && props.errors.lastName}
          validateStatus={
              props.touched.lastName && props.errors.lastName ? 'error' : undefined
          }>
          <Input
              id={'lastName'}
              name={'lastName'}
              placeholder={'Last Name'}
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.lastName}/>
      </FormItem>

      <DateTimePicker values={props.values}
                      errors={props.errors}
                      touched={props.touched}
                      handleChange={props.handleChange}
                      handleBlur={props.handleBlur}
                      setFieldValue={props.setFieldValue}/>
  </React.Fragment>
}

const UserInfo = (props) => {
    return <Row>
                {Form1(props)}
                <Row className="btns">
                    <Col xs={12}>
                        <Button onClick={(e) => {backBtn(e, props)} }>Cancel</Button>
                    </Col>
                    <Col xs={12}>
                        <Button type="primary" onClick={(e) => {nextBtn(e, props)}}>Next</Button>
                    </Col>
                </Row>
        </Row>
}

const UserForm1 = connect(
)(UserInfo)

export {
  Form1,
  UserForm1
}
