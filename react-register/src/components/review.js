import React from 'react'
import { Col, Row, Button } from 'antd';
import './review.css'
import {connect} from 'react-redux'
import {next, back} from './actions'
import {Form1} from './form1'
import {Form2} from './form2'

const options = [
    { label: 'Blogging', value: 'blogging' },
    { label: 'Sports', value: 'sports' },
    { label: 'Art', value: 'art' },
    { label: 'Gaming', value: 'gaming' },
    { label: 'Travelling', value: 'travelling' },
    { label: 'Pet Care', value: 'pet_care' },
    { label: 'Music', value: 'music' },
    { label: 'Cooking', value: 'cooking' }
];

const backBtn = (e, props) => {
  e.preventDefault();
  props.dispatch(back())
}

const nextBtn = (e, props) => {
  e.preventDefault();
  localStorage.setItem('details', JSON.stringify(props.values))
  props.dispatch(next())
}

const sections = [
  {title: 'User Info', name: 'userinfo'},
  {title: 'User Interests', name: 'userinterests'}
]

class Review extends React.Component {
  constructor(props) {
    super(props)

    const details = localStorage.getItem('details')
    this.state = {
      details: JSON.parse(details),
      type: ''
    }
    this.edit = this.edit.bind(this)
  }

  edit(e, type) {
    this.setState({
      type: type
    })
  }

  cancel(e) {
    e.preventDefault();
    this.setState({
      type: ''
    })
  }

  update(e) {
    e.preventDefault();
    localStorage.setItem('details', JSON.stringify(this.props.values))

    const details = localStorage.getItem('details')
    this.setState({
      details: JSON.parse(details),
      type: ''
    })
  }

  renderUpdate() {
      return <Row className="btns">
          <Col xs={12}>
              <Button onClick={(e) => {this.cancel(e)}}>Cancel</Button>
          </Col>
          <Col xs={12}>
              <Button type="primary" onClick={(e) => {this.update(e)}}>Update</Button>
          </Col>
      </Row>
  }

  renderEditableComponent(component) {
    console.log(component)
    switch(component) {
      case 'userinfo':
        return <div>
            {Form1(this.props)}
        </div>
      case 'userinterests':
        return <div>
            {Form2(this.props)}
        </div>

      default:
        return ''
    }
  }

  renderContents(name) {
      switch(name){
        case 'userinfo':
          return <div>
                    <ul>
                      <li>{this.state.details.firstName}</li>
                      <li>{this.state.details.lastName}</li>
                    </ul>
                  </div>
        case 'userinterests':
          return <div>
                    <ul>
                      {
                        this.state.details.interests.map((interest, index) => {
                          return options.map(option => {
                            if (option.value === interest) {
                              return <li key={index}>{option.label}</li>
                            } else {
                              return ''
                            }
                          })

                        })
                      }
                    </ul>
                  </div>
        default:
            return ''
      }
  }

  contentTiles(index, tile) {
    if (this.state.type === tile.name) {
      return <Row key={index}>
        {this.renderEditableComponent(tile.name)}
        <hr />
        {this.renderUpdate()}
      </Row>
    } else {
      return <Row key={index}>
        {this.renderContents(tile.name)}
        <hr />
        <Button onClick={(e) => {this.edit(e, tile.name)}}>Edit {` ${tile.title.toLowerCase()}`}</Button>
      </Row>
    }
  }

  renderContentTile() {
        const tiles = []
        sections.map((section, index) => {
            return tiles.push(this.contentTiles(index, section))
        })
        return tiles
    }

  render() {
    return <Row>
              {this.renderContentTile()}
              <Row className="btns">
                  <Col xs={12}>
                      <Button onClick={(e) => {backBtn(e, this.props)} }>Cancel</Button>
                  </Col>
                  <Col xs={12}>
                      <Button type="primary" onClick={(e) => {nextBtn(e, this.props)}}>Next</Button>
                  </Col>
              </Row>
        </Row>
  }
}

export default connect(
)(Review)
