import React from 'react'
import './datetimepicker.css'
import FormItem from 'antd/lib/form/FormItem';
import {Col, Row, DatePicker, TimePicker} from 'antd';
import {FieldArray} from 'formik';
import moment from 'moment'

const dateTimePicker = (index, props, inspection, arrayHelpers) => {
    const format = 'HH:mm';

    return (
        <div key={index}>
            <FormItem
                className="datetimepicker"
                labelCol={{xs: {span: 24}, sm: {span: 24}}}
                wrapperCol={{xs: {span: 24}, sm: {span: 24}}}
                label={'Inspection time'}
                help={props.touched.date && props.errors.date}
                validateStatus={
                    props.touched.date && props.errors.date ? 'error' : undefined
                }>
                <DatePicker
                    style={{width: '100%'}}
                    id={'date'}
                    placeholder="Select day"
                    onChange={(date, dateString) => {
                        arrayHelpers.replace(index, {
                            date: dateString,
                            from: inspection.from,
                            to: inspection.to,
                            checked: inspection.checked
                        });
                    }}
                    value={moment(new Date(inspection.date))}
                />
            </FormItem>

            <FormItem
                style={{width: '50%'}}
                className="timepicker"
                help={props.touched.from && props.errors.from}
                validateStatus={
                    props.errors.from ? 'error' : undefined
                }>
                <TimePicker style={{width: '100%'}}
                            placeholder="From"
                            format={format}
                            onChange={(date, dateString) => {
                                arrayHelpers.replace(index, {
                                    date: inspection.date,
                                    from: dateString,
                                    to: inspection.to,
                                    checked: inspection.checked
                                });
                            }}
                            value={moment(inspection.from, format)}/>
            </FormItem>
            <FormItem
                style={{width: '50%'}}
                className="timepicker"
                help={props.touched.to && props.errors.to}
                validateStatus={
                    props.errors.to ? 'error' : undefined
                }>
                <TimePicker style={{width: '100%'}}
                            placeholder="To"
                            format={format}
                            onChange={(date, dateString) => {
                                arrayHelpers.replace(index, {
                                    date: inspection.date,
                                    from: inspection.from,
                                    to: dateString,
                                    checked: inspection.checked
                                });
                            }}
                            value={moment(inspection.to, format)} />
            </FormItem>
        </div>
    )
}

const add = (e, arrayHelpers) => {
    e.preventDefault();
    const plusMinutes = new Date(new Date().getTime() + (30 * 60 * 1000))
    const format = 'HH:mm';
    arrayHelpers.push({
        date: moment(new Date()).format('YYYY-MM-DD'),
        from: moment(new Date()).format(format),
        to: moment(plusMinutes).format(format),
        checked: false
    })
}

const remove = (e, index, arrayHelpers) => {
    e.preventDefault();
    arrayHelpers.remove(index)
}

const renderSchedules = (props) => {
    return (
        <FieldArray name="inspection_times" render={arrayHelpers => (
            <div className="schedule">
                {
                    props.values.inspection_times.map((inspection, index) => {
                        return (
                            <Row key={index} className="inspection-time">
                                <Col xs={20} sm={20}>{ dateTimePicker(index, props, inspection, arrayHelpers) }</Col>
                                <Col xs={2} sm={2}>
                                    <button className="close" onClick={(e) => remove(e, index, arrayHelpers)}>
                                        +
                                    </button>
                                </Col>
                            </Row>
                        )
                    })
                }
                <button className="add blue-link" onClick={(e) => {add(e, arrayHelpers)}} >
                    Add inspection time
                </button>
            </div>
        )
        } />
    )
}

class DateTimePicker extends React.Component<DateTimePickerProps>{
    render() {
        return <div>
            { renderSchedules(this.props) }
        </div>
    }
}

export default DateTimePicker
