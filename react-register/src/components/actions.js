const next = () => ({
  type: 'next'
})

const back = () => ({
  type: 'back'
})

const edit = (component) => ({
    type: 'edit',
    component
})

export {
  next,
  back,
  edit
}
