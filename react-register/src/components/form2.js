import React from 'react'
import { Checkbox, Col, Row, Button } from 'antd';
import {connect} from 'react-redux'
import {next, back} from './actions'

const CheckboxGroup = Checkbox.Group;

const options = [
    { label: 'Blogging', value: 'blogging' },
    { label: 'Sports', value: 'sports' },
    { label: 'Art', value: 'art' },
    { label: 'Gaming', value: 'gaming' },
    { label: 'Travelling', value: 'travelling' },
    { label: 'Pet Care', value: 'pet_care' },
    { label: 'Music', value: 'music' },
    { label: 'Cooking', value: 'cooking' }
];


const backBtn = (e, props) => {
  e.preventDefault();
  localStorage.setItem('details', JSON.stringify(props.values))
  props.dispatch(back())
}

const nextBtn = (e, props) => {
  e.preventDefault();
  localStorage.setItem('details', JSON.stringify(props.values))
  props.dispatch(next())
}

const Form2 = (props) => {
  return <React.Fragment>
  <CheckboxGroup options={options}
                defaultValue={props.values.interests}
                value={props.values.interests}
                onChange={(value) => props.setFieldValue('interests', value)}/>
  </React.Fragment>
}

const UserInterests = (props) => {
    return <React.Fragment>
              {Form2(props)}
               <Row className="btns">
                   <Col xs={12}>
                       <Button onClick={(e) => {backBtn(e, props)} }>Cancel</Button>
                   </Col>
                   <Col xs={12}>
                       <Button type="primary" onClick={(e) => {nextBtn(e, props)}}>Next</Button>
                   </Col>
               </Row>
        </React.Fragment>
}

const UserForm2 = connect(
)(UserInterests)

export {
  Form2,
  UserForm2
}
// export default userInterests
