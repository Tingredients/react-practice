const steps = (state = 0, action) => {
  switch(action.type) {
    case 'next':
        return state + 1
    case 'back':
        return state - 1
    default:
        return state
  }
}
export default steps
