import React, { Component } from 'react';
import 'antd/dist/antd.css'
import './App.css'
import {Form, Col, Row, Steps, Button} from 'antd';
import {Formik} from 'formik';
import {UserForm1, UserForm2, Review} from './components'
import {connect} from 'react-redux'
import {next} from './components/actions'
import Params from './params'
import * as Yup from 'yup';
import moment from 'moment'

const steps = [
    {
        title: 'Form 1',
        element: 'form1'
    },
    {
        title: 'Form 2',
        element: 'form2'
    },
    {
        title: 'Review',
        element: 'review'
    }
];

const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .required('Required'),
  lastName: Yup.string()
    .required('Required'),
  inspection_times: Yup.array()
    .of(
      Yup.object().shape({
        from: Yup.date().required('Required'), // these constraints take precedence
        to: Yup.date()
                .when('from', (st, schema) => {
                  return Yup.date().min(st);
                }) // these constraints take precedence
      })
    )
    .required('Schedule is required') // these constraints are shown if and only if inner constraints are satisfied
});

class App extends Component {

  renderSteps() {
      return (
          <Steps
              current={this.props.step}
              direction="horizontal"
              labelPlacement="vertical">
              {steps.map(item => <Steps.Step key={item.title} title={item.title} />)}
          </Steps>
      );
  };

  switchSteps(step, values, errors, touched, handleChange, handleBlur, setFieldValue, handleSubmit) {
        switch(steps[step].element) {
            case 'form1':
                return <UserForm1 values={values}
                              errors={errors}
                              touched={touched}
                              handleChange={handleChange}
                              handleBlur={handleBlur}
                              setFieldValue={setFieldValue}/>
            case 'form2':
                return <UserForm2 values={values}
                              setFieldValue={setFieldValue}/>
            case 'review':
                return <Review values={values}
                              errors={errors}
                              touched={touched}
                              handleChange={handleChange}
                              handleBlur={handleBlur}
                              setFieldValue={setFieldValue}/>
            default:
                return ''
        }
    }

  initialValues() {
    const details = localStorage.getItem('details')

    if (details !== null) {
        Object.keys(JSON.parse(details)).map(key => {
            return Params[key] = JSON.parse(details)[key]
        })
    } else {
        return Params
    }
    return Params
  }

  handleSubmit(values, setSubmitting) {
    setSubmitting(true)

    localStorage.setItem('details', JSON.stringify(values))
    this.props.dispatch(next())
  }

  render() {
    return (
      <div className="App">
        { this.renderSteps() }
         <Row
              gutter={24}
              type={'flex'}
              align={'middle'}
              justify={'center'}>
              <Col xs={24} sm={24} md={24} lg={20} xl={16}>
                  <Formik
                      initialValues={this.initialValues()}
                      validationSchema={SignupSchema}
                      onSubmit={(values, {setSubmitting}) => {
                          this.handleSubmit(values, setSubmitting);
                      }}
                      render={({
                               values,
                               errors,
                               touched,
                               handleChange,
                               handleBlur,
                               setFieldValue,
                               handleSubmit
                           }) => {
                          return (
                              <Form onSubmit={handleSubmit}>
                                  { this.switchSteps(this.props.step, values, errors, touched, handleChange, handleBlur, setFieldValue, handleSubmit) }
                                  <Button htmlType={"submit"} type="primary">Submit</Button>
                              </Form>
                          );
                      }}
                  />
              </Col>
          </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    step: state
  }
}

export default connect(
  mapStateToProps
)(App)

// export default App;
